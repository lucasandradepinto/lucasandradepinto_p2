import copy

def main2():
    matriz = []
    matriz = [0] * 2

    for i in range(2):
        matriz[i] = [0] * 2
    a = float(input('matriz [0][0]: '))
    b = float(input('matriz [0][1]: '))
    c = float(input('matriz [1][0]: '))
    d = float(input('matriz [1][1]: '))
    matriz[0][0] = a
    matriz[0][1] = b
    matriz[1][0] = c
    matriz[1][1] = d
    print(matriz, end=('\t'))

    x = inversa(matriz)

    print(x, end=('\t'))


def inversa(matriz_normal):


    matriz = copy.deepcopy(matriz_normal)

    det_matriz = matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0]

    matriz[0][0], matriz[1][1] = matriz[1][1], matriz[0][0]

    if det_matriz == 0:
        return None

    det = 1 / det_matriz

    matriz[0][1] = -matriz[0][1]
    matriz[1][0] = -matriz[1][0]

    for linha in range(2):
        matriz[linha][0] *= det
        matriz[linha][1] *= det

    return matriz


main2()

    